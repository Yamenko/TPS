// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../TPSGameInstance.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial) {
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}

}

void ATPSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
	PlayerInputComponent->BindAxis(TEXT("MouseWeel"), this, &ATPSCharacter::SetNewArmLength);

	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);

}

void ATPSCharacter::SetNewArmLength(float Value)
{
	//UE_LOG(LogTemp, Warning, TEXT("The float value is: %f"), Value);

	if ((Value < 0 && GetCameraBoom()->TargetArmLength > MinLengthSpringArm) ||
		(Value > 0 && GetCameraBoom()->TargetArmLength < MaxLengthSpringArm)) {

		GetCameraBoom()->TargetArmLength+= Value;

	}
}

bool ATPSCharacter::CanRun()
{
	CalculateAngleBetweenVectors();
	return (Stamina > 0 && abs(AngleBetwenVectors) < 20); // �������������� �������� ���������� ����������� �������� � �������
}

bool ATPSCharacter::CanStartRun()
{
	CalculateAngleBetweenVectors();
	return (Stamina > 100 && abs(AngleBetwenVectors) < 20);  // �������������� �������� ���������� ����������� �������� � �������
}

void ATPSCharacter::ChangeStamina(float Value)
{
	Stamina += Value;
}

void ATPSCharacter::CalculateAngleBetweenVectors()
{
	// �������� ���� ����� ������������ �������� � ������������ �������
	FVector LookVector = GetActorForwardVector();	// ����������� ������� (����� ��������� �������)
	FVector RunVector = GetVelocity();				// ����������� ��������
	RunVector.Normalize();							// ����������� ������ (�������� ��������� ������)


	// ������ ���� 
	AngleBetwenVectors = acos(Dot3(LookVector, RunVector)) * 180 / PI;
	UE_LOG(LogTemp, Warning, TEXT("Angle between Look and Move vector: %f"), AngleBetwenVectors);
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATPSCharacter::InputAxisX(float Value) {
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value) {
	AxisY = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	// Movement
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	// Rotation
	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		//MyController->GetHitResultUnderCursorByChannel(TraceTypeQuery6, false, ResultHit);
		MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
		NewRotation.Pitch = 0.0f;
		NewRotation.Roll = 0.0f;

		SetActorRotation(NewRotation);	

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (CurrentMovementState)
			{
			case EMovementState::Aim_state:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Walk_state:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Run_state:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			default:
				break;
			}

			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}

	// Check Run state
	if (CurrentMovementState == EMovementState::Run_state) {
		if (CanRun()) {
			ChangeStamina(-1.0f);
			//UE_LOG(LogTemp, Warning, TEXT("The current stamina value : %f"), Stamina);
		}
		else {
			ChangeMovementState(EMovementState::Walk_state);
		}
	}
	else if (Stamina < 1000) {
		ChangeStamina(1.0f);
		//UE_LOG(LogTemp, Warning, TEXT("The current stamina value : %f"), Stamina);
	}


}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}


void ATPSCharacter::CharacterUpdate()
{
	float NewSpeed = 600.0f;

	switch (CurrentMovementState)
	{
	case EMovementState::Aim_state:
		NewSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_state:
		NewSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_state:
		if (CanStartRun()) {
			NewSpeed = MovementInfo.RunSpeed;
		}
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;

}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	CurrentMovementState = NewMovementState;
	CharacterUpdate();
}
	
AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::InitWeapon(FName IdWeaponName)
{
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					CurrentWeapon = myWeapon;

					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(CurrentMovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFire.AddDynamic(this, &ATPSCharacter::WeaponFire);

					
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}


}

void ATPSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon){
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim) { WeaponReloadStart_BP(Anim); }
void ATPSCharacter::WeaponFire(UAnimMontage* Anim){ WeaponFire_BP(Anim);}
void ATPSCharacter::WeaponReloadEnd(){	WeaponReloadEnd_BP();}
void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim) {} // in BP
void ATPSCharacter::WeaponFire_BP_Implementation(UAnimMontage* Anim){} // in BP
void ATPSCharacter::WeaponReloadEnd_BP_Implementation() {} // in BP


